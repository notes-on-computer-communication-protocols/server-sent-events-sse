# Server-Sent Events SSE

EventSource, or Server-Sent Events is an HTML5 server push technology enabling a client to receive automatic updates from a server via HTTP connection. https://en.m.wikipedia.org/wiki/Server-sent_events

* [Can I use _?](https://caniuse.com/#feat=eventsource)
* [*Polling vs SSE vs WebSocket— How to choose the right one*
  ](https://codeburst.io/polling-vs-sse-vs-websocket-how-to-choose-the-right-one-1859e4e13bd9)
  2018-07 Bharathvaj Ganesan

# Libraries for web-sites back-ends

## Elixir

## Haskell
* [*Servant Server Sent Events support*
  ](https://stackoverflow.com/questions/37860770/servant-server-sent-events-support)
  (2018)

### wai-extra
* https://www.stackage.org/package/wai-extra
* Used by https://www.stackage.org/package/yesod-eventsource

### base
* https://hackage.haskell.org/package/base-4.10.1.0/docs/Control-Concurrent-Chan.html#t:Chan

## Perl
### Mojolicious

## PHP
### symfony/mercure 38k
### igorw/event-source 2k
### hoa/eventsource 4k

## Python

## Rust
### wrap
* https://crates.io/crates/warp